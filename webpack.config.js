var debug = process.env.NODE_ENV !== "production";
var webpack = require('webpack');

module.exports = {
  context: __dirname,
  devtool: debug ? "inline-sourcemap" : false,
  devServer: { inline: true },
  entry: "./script.js",
  output: {
    path: __dirname + "./",
    filename: "script.min.js"
  },
  module: {
  loaders: [
    {
    //  test: /\.js$/,
      exclude: /(node_modules|bower_components)/,
      loader: 'babel-loader',
      query: {
        presets: ['es2015']
      }
    }
  ]
},
  plugins: debug ? [] : [
    new webpack.optimize.UglifyJsPlugin({ mangle: true, sourcemap: false }),
  ],
};
